package com.benkyou.ecommerce.mapper;

import com.benkyou.ecommerce.dto.PriceDTO;
import com.benkyou.ecommerce.entity.Price;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class PriceMapper {

    public static PriceDTO toDTO(Price price) {
        return new PriceDTO()
                .builder()
                .rrpId(price.getPriceList())
                .brandId(price.getBrandId())
                .productId(price.getProductId())
                .startDate(price.getStartDate())
                .endDate(price.getEndDate())
                .recommendedRetailPrice(price.getPrice())
                .currency(price.getCurrency())
                .build();
    }

}
