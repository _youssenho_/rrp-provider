package com.benkyou.ecommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "PRICES")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "Price.findRecommendedRetailPriceBetweenTwoDates",
        query = "select p from Price p where p.brandId = ?1 " +
                "and p.productId = ?2 " +
                "and (?3 between p.startDate and p.endDate) " +
                "order by p.priority desc")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int priceList;
    private int brandId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private int productId;
    private int priority;
    private Double price;
    private String currency;

}
