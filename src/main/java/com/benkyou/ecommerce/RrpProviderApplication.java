package com.benkyou.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RrpProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(RrpProviderApplication.class, args);
	}

}
