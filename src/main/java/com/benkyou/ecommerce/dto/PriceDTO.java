package com.benkyou.ecommerce.dto;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PriceDTO {

    private int rrpId;
    private int brandId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private int productId;
    private Double recommendedRetailPrice;
    private String currency;

}
