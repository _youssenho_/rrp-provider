package com.benkyou.ecommerce.repository;

import com.benkyou.ecommerce.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface PriceRepository extends JpaRepository<Price, Integer> {

    List<Price> findRecommendedRetailPriceBetweenTwoDates(int brandId, int productId, LocalDateTime applicationDate);

}