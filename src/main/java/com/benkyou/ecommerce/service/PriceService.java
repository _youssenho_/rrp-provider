package com.benkyou.ecommerce.service;

import com.benkyou.ecommerce.entity.Price;
import com.benkyou.ecommerce.exception.PriceNotFoundException;
import com.benkyou.ecommerce.repository.PriceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static java.time.LocalDateTime.parse;

@Service
@AllArgsConstructor
public class PriceService {

    private PriceRepository priceRepo;

    public Price getRecommendedRetailPrice(int brandId, int productId, String applicationDate) {
        return priceRepo.findRecommendedRetailPriceBetweenTwoDates(brandId, productId, parse(applicationDate))
                .stream()
                .findFirst()
                .orElseThrow(() -> new PriceNotFoundException(
                        String.format("Recommended Retail Price not found for brandId=%d, productId=%d and applicationDate=%s",
                                brandId, productId, applicationDate)));
    }

}
