package com.benkyou.ecommerce.controller;

import com.benkyou.ecommerce.dto.ErrorDTO;
import com.benkyou.ecommerce.dto.PriceDTO;
import com.benkyou.ecommerce.exception.PriceNotFoundException;
import com.benkyou.ecommerce.service.PriceService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.format.DateTimeParseException;

import static com.benkyou.ecommerce.mapper.PriceMapper.toDTO;

@RestController
@AllArgsConstructor
public class PriceController {

    private PriceService priceService;

    @GetMapping("/v1/rrp")
    public ResponseEntity<PriceDTO> getRecommendedRetailPrice(
            @RequestParam int brandId,
            @RequestParam int productId,
            @RequestParam String applicationDate) {
        return new ResponseEntity<>(
                toDTO(priceService.getRecommendedRetailPrice(brandId, productId, applicationDate)),
                HttpStatus.OK
        );
    }

    @ExceptionHandler(PriceNotFoundException.class)
    public ResponseEntity<ErrorDTO> priceNotFoundError(PriceNotFoundException exception) {
        return getResponseEntity(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<ErrorDTO> applicationDateFormatError(DateTimeParseException exception) {
        return getResponseEntity(exception, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ErrorDTO> getResponseEntity(RuntimeException exception, HttpStatus status) {
        return new ResponseEntity<>(new ErrorDTO().builder().errorDescription(exception.getMessage()).build(), HttpStatus.BAD_REQUEST);
    }

}
