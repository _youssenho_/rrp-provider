package com.benkyou.ecommerce.service;

import com.benkyou.ecommerce.entity.Price;
import com.benkyou.ecommerce.exception.PriceNotFoundException;
import com.benkyou.ecommerce.repository.PriceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.benkyou.ecommerce.util.ObjectTestFactory.getRecommendedRetailPriceExample;
import static java.time.LocalDateTime.parse;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PriceServiceTest {

    private PriceRepository priceRepository;
    private PriceService priceService;

    @BeforeEach
    public void setup() {
        priceRepository = mock(PriceRepository.class);
        priceService = new PriceService(priceRepository);
    }

    @Test
    public void Given_RequestForRecommendedRetailPrice_When_RrpIsAvailable_Then_ReturnIt() {
        //GIVEN
        int brandId = 1;
        int productId = 123;
        String applicationDate = "2020-06-14T18:30:00";

        Price rrpHighPriority = getRecommendedRetailPriceExample();

        Price rrpLessPriority = getRecommendedRetailPriceExample();
        rrpLessPriority.setPriority(0);

        when(priceRepository.findRecommendedRetailPriceBetweenTwoDates(eq(brandId), eq(productId), eq(parse(applicationDate))))
                .thenReturn(Arrays.asList(rrpHighPriority, rrpLessPriority));

        //WHEN
        Price recommendedRetailPrice = priceService.getRecommendedRetailPrice(brandId, productId, applicationDate);

        //THEN
        assertThat(recommendedRetailPrice).isEqualTo(rrpHighPriority);
    }

    @Test
    public void Given_RequestForRecommendedRetailPrice_When_RrpIsNotAvailable_Then_ThrowAnException() {
        //GIVEN
        int brandId = 1;
        int productId = 123;
        String applicationDate = "2020-06-14T18:30:00";
        when(priceRepository.findRecommendedRetailPriceBetweenTwoDates(eq(brandId), eq(productId), eq(parse(applicationDate))))
                .thenReturn(new ArrayList<>());

        //THEN
        assertThatExceptionOfType(PriceNotFoundException.class).isThrownBy(
                //WHEN
                () -> priceService.getRecommendedRetailPrice(brandId, productId, applicationDate)
        ).withMessageContaining(
                String.format("Recommended Retail Price not found for brandId=%d, productId=%d and applicationDate=%s",
                        brandId, productId, applicationDate));
    }

    @Test
    public void Given_RequestForRecommendedRetailPriceWithWrongDateFormat_When_RrpIsRequested_Then_ThrowAnException() {
        //GIVEN
        int brandId = 1;
        int productId = 123;
        String applicationDate = "2020/06/14T18:30:00";
        when(priceRepository.findRecommendedRetailPriceBetweenTwoDates(eq(brandId), eq(productId), any()))
                .thenReturn(new ArrayList<>());

        //THEN
        assertThatExceptionOfType(DateTimeParseException.class).isThrownBy(
                //WHEN
                () -> priceService.getRecommendedRetailPrice(brandId, productId, applicationDate)
        ).withMessageMatching("Text .* could not be parsed .*");
    }

}