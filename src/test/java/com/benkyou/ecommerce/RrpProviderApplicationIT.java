package com.benkyou.ecommerce;

import com.benkyou.ecommerce.dto.ErrorDTO;
import com.benkyou.ecommerce.dto.PriceDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RrpProviderApplicationIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;


    /*
    |BRAND_ID|START_DATE         |END_DATE           |PRICE_LIST|PRODUCT_ID|PRIORITY|PRICE|CURR|
    |--------|-------------------|-------------------|----------|----------|--------|-----|----|
    |1       |2020-06-14-00.00.00|2020-12-31-23.59.59|1         |35455     |0       |35.50|EUR |
    |1       |2020-06-14-15.00.00|2020-06-14-18.30.00|2         |35455     |1       |25.45|EUR |
    |1       |2020-06-15-00.00.00|2020-06-15-11.00.00|3         |35455     |1       |30.50|EUR |
    |1       |2020-06-15-16.00.00|2020-12-31-23.59.59|4         |35455     |1       |38.95|EUR |

    - Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA) --> tarifa 1
    - Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA) --> tarifa 2
    - Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA) --> tarifa 1
    - Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA) --> tarifa 3
    - Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA) --> tarifa 4
     */
    @Test
    public void testCases() {
        testCase(1, 35.50, 1, 35455, "2020-06-14T10:00:00");
        testCase(2, 25.45, 1, 35455, "2020-06-14T16:00:00");
        testCase(1, 35.50, 1, 35455, "2020-06-14T21:00:00");
        testCase(3, 30.50, 1, 35455, "2020-06-15T10:00:00");
        testCase(4, 38.95, 1, 35455, "2020-06-16T21:00:00");
    }

    private void testCase(int expectedRrpId, Double expectedPrice, int brandId, int productId, String applicationDate) {
        ResponseEntity<PriceDTO> response = restTemplate.getForEntity(createRrpEndpointUrl(brandId, productId, applicationDate), PriceDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        PriceDTO recommendedRetailPrice = response.getBody();
        assertThat(recommendedRetailPrice.getRrpId()).isEqualTo(expectedRrpId);
        assertThat(recommendedRetailPrice.getRecommendedRetailPrice()).isEqualTo(expectedPrice);
    }

    @Test
    public void failureTestCases() {
        failureTestCase("Recommended Retail Price not found for .*", 2, 35455, "2020-06-14T10:00:00");
        failureTestCase("Text .* could not be parsed .*", 2, 35455, "2020-06-14T");
    }

    private void failureTestCase(String expectedDescriptionRegex, int brandId, int productId, String applicationDate) {
        ResponseEntity<ErrorDTO> response = restTemplate.getForEntity(createRrpEndpointUrl(brandId, productId, applicationDate), ErrorDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        assertThat(response.getBody().getErrorDescription()).matches(expectedDescriptionRegex);
    }

    private String createRrpEndpointUrl(int brandId, int productId, String applicationDate) {
        return createBaseUrl().concat(String.format("/v1/rrp?brandId=%d&productId=%d&applicationDate=%s", brandId, productId, applicationDate));
    }

    private String createBaseUrl() {
        return String.format("http://localhost:%d", port);
    }
}
