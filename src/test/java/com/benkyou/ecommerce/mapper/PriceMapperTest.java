package com.benkyou.ecommerce.mapper;

import com.benkyou.ecommerce.dto.PriceDTO;
import com.benkyou.ecommerce.entity.Price;
import org.junit.jupiter.api.Test;

import static com.benkyou.ecommerce.util.ObjectTestFactory.getRecommendedRetailPriceExample;
import static org.assertj.core.api.Assertions.assertThat;

public class PriceMapperTest {

    @Test
    public void toDTOTest() {
        //GIVEN
        Price recommendedRetailPrice = getRecommendedRetailPriceExample();

        //WHEN
        PriceDTO priceDTO = PriceMapper.toDTO(recommendedRetailPrice);

        //THEN
        assertThat(priceDTO.getRrpId()).isEqualTo(recommendedRetailPrice.getPriceList());
        assertThat(priceDTO.getBrandId()).isEqualTo(recommendedRetailPrice.getBrandId());
        assertThat(priceDTO.getStartDate()).isEqualTo(recommendedRetailPrice.getStartDate());
        assertThat(priceDTO.getEndDate()).isEqualTo(recommendedRetailPrice.getEndDate());
        assertThat(priceDTO.getProductId()).isEqualTo(recommendedRetailPrice.getProductId());
        assertThat(priceDTO.getRecommendedRetailPrice()).isEqualTo(recommendedRetailPrice.getPrice());
        assertThat(priceDTO.getCurrency()).isEqualTo(recommendedRetailPrice.getCurrency());
    }

}