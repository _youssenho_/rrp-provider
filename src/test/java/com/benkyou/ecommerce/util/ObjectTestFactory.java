package com.benkyou.ecommerce.util;

import com.benkyou.ecommerce.entity.Price;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

@UtilityClass
public final class ObjectTestFactory {

    public static Price getRecommendedRetailPriceExample() {
        int priceList = 1;
        int brandId = 1;
        LocalDateTime startDate = LocalDateTime.now();
        LocalDateTime endDate = startDate.plusDays(2);
        int productId = 12345;
        int priority = 1;
        Double price = 10.23;
        String currency = "EUR";

        return new Price()
                .builder()
                .priceList(priceList)
                .brandId(brandId)
                .startDate(startDate)
                .endDate(endDate)
                .productId(productId)
                .priority(priority)
                .price(price)
                .currency(currency)
                .build();
    }

}
