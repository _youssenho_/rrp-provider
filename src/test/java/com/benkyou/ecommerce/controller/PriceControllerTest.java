package com.benkyou.ecommerce.controller;

import com.benkyou.ecommerce.dto.ErrorDTO;
import com.benkyou.ecommerce.dto.PriceDTO;
import com.benkyou.ecommerce.entity.Price;
import com.benkyou.ecommerce.exception.PriceNotFoundException;
import com.benkyou.ecommerce.service.PriceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.format.DateTimeParseException;

import static com.benkyou.ecommerce.mapper.PriceMapper.toDTO;
import static com.benkyou.ecommerce.util.ObjectTestFactory.getRecommendedRetailPriceExample;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PriceControllerTest {

    private PriceService priceService;
    private PriceController priceController;

    @BeforeEach
    public void setup() {
        priceService = mock(PriceService.class);
        priceController = new PriceController(priceService);
    }

    @Test
    public void test_getRecommendedRetailPrice() {
        Price price = getRecommendedRetailPriceExample();

        //GIVEN
        int brandId = price.getBrandId();
        int productId = price.getProductId();
        String applicationDate = price.getStartDate().plusDays(1).toString();

        //WHEN
        when(priceService.getRecommendedRetailPrice(eq(brandId), eq(productId), eq(applicationDate))).thenReturn(price);
        ResponseEntity<PriceDTO> recommendedRetailPriceResponse = priceController.getRecommendedRetailPrice(brandId, productId, applicationDate);

        //THEN
        assertThat(recommendedRetailPriceResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(recommendedRetailPriceResponse.getBody()).isEqualTo(toDTO(price));
    }

    @Test
    public void test_priceNotFoundError() {
        ResponseEntity<ErrorDTO> response = priceController.priceNotFoundError(new PriceNotFoundException("Recommended Retail price not found"));

        assertResponseMatchesExpectedValues(response, "Recommended Retail price not found", HttpStatus.BAD_REQUEST);
    }

    @Test
    public void test_applicationDateFormatError() {
        ResponseEntity<ErrorDTO> response = priceController.applicationDateFormatError(new DateTimeParseException("Text could not be parsed", "", 0));

        assertResponseMatchesExpectedValues(response, "Text could not be parsed", HttpStatus.BAD_REQUEST);
    }

    private void assertResponseMatchesExpectedValues(ResponseEntity<ErrorDTO> response, String expectedErrorMessage, HttpStatus expectedHttpStatus) {
        assertThat(response.getStatusCode()).isEqualTo(expectedHttpStatus);
        assertThat(response.getBody().getErrorDescription()).isEqualTo(expectedErrorMessage);
    }

}